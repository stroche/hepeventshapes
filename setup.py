import numpy as np
from setuptools import setup
from setuptools.command.install import install
import atexit
from eventIsotropy.cylGen import cylinderGen
import numpy as np
import pathlib

long_description = '''
hi
'''


def _post_install():
    # generate any data you need to
    for i in (2, 4, 8, 16, 32, 64, 128, 256):
        quasiUniformCyl = cylinderGen(piSeg=128, etaMax=4.9)
        this_dir = str(pathlib.Path(__file__).parent.resolve())
        np.savetxt(this_dir + '/HEPEventShapes/quasiUniformCylinder/%s.csv' % i,
                   X=quasiUniformCyl,
                   delimiter=',')


class NewInstall(install):
    def __init__(self, *args, **kwargs):
        super(NewInstall, self).__init__(*args, **kwargs)
        atexit.register(_post_install)


setup(name='HEPEventShapes',
      version='0.0.0',
      description='hi',
      long_description=long_description,
      author='Stephen Roche, Saint Louis University',
      author_email='stephen.roche@health.slu.edu',
      url='https://gitlab.com/stroche/hepeventshapes',
      license='MIT',
      packages=['HEPEventShapes'],
      install_requires=['numpy',
                        'scipy',
                        'tqdm',
                        'vector',
                        'eventIsotropy @ git+https://github.com/caricesarotti/event_isotropy.git@v1.0.0'],
      python_requires='>=3.0',
      classifiers=[
          'Intended Audience :: Developers',
          'Intended Audience :: Science/Research',
          'Programming Language :: C++',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.6',
          'Topic :: Software Development :: Libraries',
          'Topic :: Software Development :: Libraries :: Python Modules'
      ],
      cmdclass={'install': NewInstall})
