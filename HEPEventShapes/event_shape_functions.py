from typing import List

import numpy as np
import vector
from scipy.linalg import eigh
from scipy.optimize import minimize_scalar
from eventIsotropy.emdVar import _cdist_phi_y, emd_Calc
import pathlib

this_dir = str(pathlib.Path(__file__).parent.resolve())
quasiUniformCylinderDict = {}
for spacingCount in (2, 4, 8, 16, 32, 64, 128, 256):
    quasiUniformCylinderDict[spacingCount] = np.genfromtxt(this_dir + '/quasiUniformCylinder/%s.csv' % spacingCount,
                                                           delimiter=',')


def get_jet_p(jet: vector.obj) -> float:
    return np.sqrt(np.square(jet.px) + np.square(jet.py) + np.square(jet.pz))


def get_jet_sphericity_tensor(jet: vector.obj) -> np.ndarray:
    return np.array([[np.square(jet.px), jet.px * jet.py, jet.px * jet.pz],
                     [jet.py * jet.px, np.square(jet.py), jet.py * jet.pz],
                     [jet.pz * jet.px, jet.pz * jet.py, np.square(jet.pz)]]) / get_jet_p(jet)


class Event(object):
    def __init__(self,
                 jets: List[vector.obj]):
        """
        initialize object
        """
        self.jets = jets

        # initialize properties
        self.jet_pt_sum = None
        self.transverse_thrust_axis = None
        self.transverse_thrust = None
        self.complement_of_transverse_thrust = None
        self.upper_region_jets = None
        self.lower_region_jets = None
        self.upper_jet_broadening = None
        self.lower_jet_broadening = None
        self.jet_broadening = None
        self.event_sphericity_tensor = None
        self.jet_p_sum = None
        self.event_sphericity_eigenvalues = None
        self.sphericity = None
        self.aplanarity = None
        self.c_parameter = None
        self.d_parameter = None
        self.isotropy = None

    @staticmethod
    def _get_jet_p_sum(jets: List[vector.obj]) -> float:
        return np.sum([get_jet_p(j) for j in jets])

    def get_jet_p_sum(self) -> float:
        if self.jet_p_sum is None:
            self.jet_p_sum = self._get_jet_p_sum(self.jets)
        return self.jet_p_sum

    @staticmethod
    def _get_jet_pt_sum(jets: List[vector.obj]) -> float:
        return np.sum([j.pt for j in jets])

    def get_jet_pt_sum(self) -> float:
        if self.jet_pt_sum is None:
            self.jet_pt_sum = self._get_jet_pt_sum(self.jets)
        return self.jet_pt_sum

    def get_transverse_thrust_axis(self) -> np.ndarray:
        if self.transverse_thrust_axis is None:
            axis = minimize_scalar(fun=self._thrust_axis_equation,
                                   args=self.jets,
                                   bounds=(0, 2 * np.pi))
            self.transverse_thrust_axis = np.array([np.cos(axis.x),
                                                    np.sin(axis.x)])
        return self.transverse_thrust_axis

    @staticmethod
    def _thrust_axis_equation(theta: float,
                              jets: List[vector.obj]) -> float:
        arr = [np.cos(theta), np.sin(theta)]
        out = 0.0
        for jet in jets:
            out += np.abs(np.dot([jet.px, jet.py],
                                 arr))
        return -out

    def get_transverse_thrust(self) -> float:
        if self.transverse_thrust is None:
            axis = self.get_transverse_thrust_axis()
            out = 0.0
            for jet in self.jets:
                out += np.abs(np.dot([jet.px, jet.py],
                                     axis))

            out /= self.get_jet_pt_sum()
            self.transverse_thrust = out
        return self.transverse_thrust

    def get_complement_of_transverse_thrust(self) -> float:
        if self.complement_of_transverse_thrust is None:
            self.complement_of_transverse_thrust = 1 - self.get_transverse_thrust()
        return self.complement_of_transverse_thrust

    def set_jet_regions(self) -> None:
        self.upper_region_jets = []
        self.lower_region_jets = []
        for jet in self.jets:
            if np.dot([jet.px, jet.py],
                      self.get_transverse_thrust_axis()) > 0.0:
                self.upper_region_jets.append(jet)
            else:
                self.lower_region_jets.append(jet)

    def get_upper_region_jets(self) -> List[vector.obj]:
        if self.upper_region_jets is None:
            self.set_jet_regions()
        return self.upper_region_jets

    def get_lower_region_jets(self) -> List[vector.obj]:
        if self.lower_region_jets is None:
            self.set_jet_regions()
        return self.lower_region_jets

    @staticmethod
    def _get_pt_weighted_eta(jets: List[vector.obj]) -> float:
        sum_pt = 0.0
        out = 0.0
        for jet in jets:
            out += jet.pt * jet.eta
            sum_pt += jet.pt
        return out / sum_pt

    @staticmethod
    def _get_pt_weighted_phi(jets: List[vector.obj]) -> float:
        sum_pt = 0.0
        out = 0.0
        for jet in jets:
            out += jet.pt * jet.phi
            sum_pt += jet.pt
        return out / sum_pt

    @staticmethod
    def _get_jet_broadening(jets: List[vector.obj]) -> float:
        if len(jets) == 0:
            raise Exception('No jets given to Event._get_jet_broadening')
        scalar_pt = Event._get_jet_pt_sum(jets)
        out = 0.0
        eta_X = Event._get_pt_weighted_eta(jets)
        phi_X = Event._get_pt_weighted_phi(jets)

        for jet in jets:
            out += jet.pt * np.sqrt(np.square(jet.eta - eta_X) + np.square(jet.phi - phi_X))

        return out / (2 * scalar_pt)

    def get_upper_jet_broadening(self) -> float:
        if self.upper_jet_broadening is None:
            if len(self.get_upper_region_jets()) == 0:
                print('No jets in upper region. Setting region broadening to be zero')
                self.upper_jet_broadening = 0
            else:
                self.upper_jet_broadening = self._get_jet_broadening(self.get_upper_region_jets())
        return self.upper_jet_broadening

    def get_lower_jet_broadening(self) -> float:
        if self.lower_jet_broadening is None:
            if len(self.get_lower_region_jets()) == 0:
                print('No jets in lower region. Setting region broadening to be zero')
                self.lower_jet_broadening = 0
            else:
                self.lower_jet_broadening = self._get_jet_broadening(self.get_lower_region_jets())
        return self.lower_jet_broadening

    def get_jet_broadening(self) -> float:
        if self.jet_broadening is None:
            self.jet_broadening = self.get_upper_jet_broadening() + self.get_lower_jet_broadening()
        return self.jet_broadening

    def get_event_sphericity_tensor(self) -> np.ndarray:
        if self.event_sphericity_tensor is None:
            out = np.zeros((3, 3))
            for jet in self.jets:
                out += get_jet_sphericity_tensor(jet)
            self.event_sphericity_tensor = out / self.get_jet_p_sum()
        return self.event_sphericity_tensor

    def get_event_sphericity_eigenvalues(self) -> np.ndarray:
        if self.event_sphericity_eigenvalues is None:
            # hermitian matrix --> we can use eigh instead of eigvals
            out = eigh(self.get_event_sphericity_tensor(),
                       check_finite=False,
                       eigvals_only=True)
            self.event_sphericity_eigenvalues = out
        return self.event_sphericity_eigenvalues

    def get_sphericity(self) -> float:
        if self.sphericity is None:
            l3, l2, l1 = self.get_event_sphericity_eigenvalues()
            self.sphericity = (3.0 / 2.0) * (l2 + l3)
        return self.sphericity

    def get_aplanarity(self) -> float:
        if self.aplanarity is None:
            l3, l2, l1 = self.get_event_sphericity_eigenvalues()
            self.aplanarity = (3.0 / 2.0) * l3
        return self.aplanarity

    def get_c_parameter(self) -> float:
        if self.c_parameter is None:
            l3, l2, l1 = self.get_event_sphericity_eigenvalues()
            self.c_parameter = 3 * (l1 * l2 + l1 * l3 + l2 * l3)
        return self.c_parameter

    def get_d_parameter(self) -> float:
        if self.d_parameter is None:
            self.d_parameter = 27 * np.prod(self.get_event_sphericity_eigenvalues())
        return self.d_parameter

    def get_isotropy(self,
                     i: int) -> float:
        if i not in (2, 4, 8, 16, 32, 64, 128, 256):
            raise ValueError('I must be one of the following numbers:'
                             '(2,4,8,16,32,64,128,256). You have set it to %s' % i)
        if self.isotropy is None:
            X = []
            for j in self.jets:
                # events are generated with phi between [0, 2pi]
                # here we usually have [-pi, pi]
                # therefore, gotta convert to be uniform
                phi = j.phi
                if phi < 0:
                    X.append([j.eta, (2 * np.pi) + phi])
                else:
                    X.append([j.eta, phi])
            X = np.array(X)
            M = _cdist_phi_y(X=X,
                             Y=quasiUniformCylinderDict[i],
                             ym=4.9)
            ev0 = np.array([j.pt for j in self.jets])
            ev1 = np.ones(len(quasiUniformCylinderDict[i]))
            self.isotropy = emd_Calc(ev0=ev0,
                                     ev1=ev1,
                                     M=M)
        return self.isotropy
