from HEPEventShapes import Event
import vector

my_jets = [
    vector.obj(pt=125, eta=0.4, phi=1.2),
    vector.obj(pt=25, eta=-0.4, phi=-1.2),
    vector.obj(pt=35, eta=-1.2, phi=0.4)
]

Evt = Event(my_jets)
print(Evt.get_transverse_thrust_axis())
print(Evt.get_transverse_thrust())
print(Evt.get_complement_of_transverse_thrust())
print(Evt.get_upper_region_jets())
print(Evt.get_lower_region_jets())
print(Evt.get_jet_broadening())
print(Evt.get_event_sphericity_tensor())
print(Evt.get_event_sphericity_eigenvalues())
print(Evt.get_sphericity())
print(Evt.get_aplanarity())
print(Evt.get_c_parameter())
print(Evt.get_d_parameter())
print(Evt.get_isotropy(128))
